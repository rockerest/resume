import { globbySync as glob } from "globby";
import esbuild from "esbuild";

var cwd = process.cwd();

var external = [
	"assert",
	"buffer",
	"child_process",
	"crypto",
	"dgram",
	"domain",
	"events",
	"fs",
	"http",
	"https",
	"module",
	"os",
	"path",
	"punycode",
	"querystring",
	"stream",
	"string_decoder",
	"timers",
	"tty",
	"url",
	"util",
	"zlib"
];

var functions = glob( `${cwd}/fn/fn.*.js` );

functions.forEach( async ( path ) => {
	var name = path.replace( /.*?fn\.(.*?)\.js$/, "$1" );

	esbuild.buildSync( {
		"entryPoints": [ path ],
		"bundle": true,
		"format": "esm",
		"platform": "node",
		"outfile": `${cwd}/functions/${name}.js`,
		external
	} );
} );
