import { load } from "./data/Short.js";

export default async ( Req ) => {
	var params = new URL( Req.url ).searchParams;
	var path = params.get( "code" ) || null;
	var root = params.get( "domain" ) || "https://rdl.ph";
	var headers = {};
	var body = {};
	var fwd;

	headers.location = root;
	body.statusCode = 307;

	if( path ){
		fwd = await load( path );
		
		if( fwd && fwd.url ){
			headers.location = fwd.url;
			body.statusCode = 308;
		}
	}

	return new Response( JSON.stringify( body ), { headers } );
}