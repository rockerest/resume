import {
	DynamoDBClient,
	GetItemCommand,
	// QueryCommand,
	// BatchGetItemCommand,
	// DeleteItemCommand,
	// PutItemCommand,
	// ScanCommand,
	// UpdateItemCommand
} from "@aws-sdk/client-dynamodb";
import { marshall, unmarshall } from "@aws-sdk/util-dynamodb";

function isObject( result ){
	var is = typeof result == "object";

	return result &&
		is &&
		!Array.isArray( result );
}

function objectResult( result ){
	return isObject( result ) && Boolean( Object.keys( result ).length );
}

function hasResult( result ){
	return	Boolean(
		result ||
		Array.isArray( result ) ||
		objectResult( result )
	);
}

function getResult( result ){
	var unmarshalled = result;

	if( Array.isArray( result ) ){
		unmarshalled = result.map( unmarshall );
	}
	else if( objectResult( result ) ){
		unmarshalled = unmarshall( result );
	}

	return unmarshalled;
}

function unpackDynamoData( data, method ){
	// Fuck AWS for having a unique key for every method
	var extractors = {
		// "batchGet": [ "Responses", {} ],
		// "delete": [ "Attributes", null ],
		"get": [ "Item", null ]
		// "put": [ "Attributes", null ],
		// "query": [ "Items", [] ],
		// "scan": [ "Items", [] ],
		// "update": [ "Attributes", null ]
	};
	var extractor = extractors[ method ];
	var result = data[ extractor[ 0 ] ];

	return hasResult( result ) ? getResult( result ) : extractor[ 1 ];
}

export function getDocumentClient( { key = process.env.AWS_SHORT_KEY, secret = process.env.AWS_SHORT_SECRET } = {} ){
	return new DynamoDBClient( {
		"endpoint": "https://dynamodb.us-west-2.amazonaws.com",
		"region": "us-west-2",
		"credentials": {
			"accessKeyId": key,
			"secretAccessKey": secret
		}
	} );
}

export async function dynamo( client, method, params ){
	var commands = {
		"get": ( p ) => new GetItemCommand( p ),
		// "query": ( p ) => new QueryCommand( p ),
		// "batchGet": ( p ) => new BatchGetItemCommand( p ),
		// "delete": ( p ) => new DeleteItemCommand( p ),
		// "put": ( p ) => new PutItemCommand( p ),
		// "scan": ( p ) => new ScanCommand( p ),
		// "update": ( p ) => new UpdateItemCommand( p )
	};
	var cmd = commands[ method ]( {
		"TableName": "short",
		...params
	} );
	var response;
	var data;

	try{
		data = await client.send( cmd );

		response = unpackDynamoData( data, method );
	}
	catch( error ){
		response = error;
	}

	return response;
}

export { marshall };