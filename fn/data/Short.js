import { dynamo, getDocumentClient, marshall } from "../common/DynamoDB.js";

export async function load( code ){
	var client = getDocumentClient();
	var data = await dynamo( client, "get", {
		"Key": marshall( {
			code
		} )
	} );

	return data || null;
}